package testes;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import br.com.nws.SAXParseExceptionTranslate.XMLErrorHandler;
import br.com.nws.SAXParseExceptionTranslate.XMLHandler;

public class LendoXML {

	private static Document dom;
	private static DocumentBuilderFactory builderFactory;
	private static String xml = "upload/faturamento.xml";
	private static File xsd = new File("schema/tissV2_02_03.xsd");

	public static void main(String[] args) {

		builderFactory = DocumentBuilderFactory.newInstance();

		SAXSource source = new SAXSource(new InputSource(xml));

		SchemaFactory schemaFactory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		XMLErrorHandler erroHandler = new XMLErrorHandler();

		try {
			Schema schema = schemaFactory.newSchema(xsd);

			Validator validator = schema.newValidator();
			validator.setErrorHandler(erroHandler);
			validator.validate(source);

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (erroHandler.getErrors().size() > 0) {

			for (SAXParseException exception : erroHandler.getErrors()) {
				if (exception != null)
					System.err.println(exception.getMessage() + " Linha: "
							+ exception.getLineNumber());
			}

		} else {

			parseDocumento();

		}
	}

	public static void parseXmlFile() {
		DocumentBuilder builder;
		try {
			builder = builderFactory.newDocumentBuilder();

			dom = builder.parse(xml);

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void parseDocumento() {

		SAXParserFactory parserFactory = SAXParserFactory.newInstance();

		try {
			SAXParser saxParser = parserFactory.newSAXParser();

			saxParser.parse(xml, new XMLHandler());
			
			System.out.println(saxParser);

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}