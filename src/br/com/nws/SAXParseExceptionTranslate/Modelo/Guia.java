package br.com.nws.SAXParseExceptionTranslate.Modelo;

import java.sql.Date;
import java.util.List;

public class Guia {

	private String registroANSFontePagadora;
	private Date dataEmissaoGuia;
	private String numeroGuiaPrestador;
	private String numeroGuiaOperadora;
	private String numeroGuiaSolicitacao;
	private String numerocarteira;
	private String nomebeneficiario;
	private String nomePlano;
	private String codigoPrestadorOperadora;
	private String nomeContratado;
	private String numeroCNES;
	private String senhaAutorizacao;
	private String caraterInternacao;
	private String acomodacao;
	private Date dataHoraInternacao;
	private Date dataHoraSaidaInterncao;
	private Integer tipoInternacao;
	private Integer regimeInternacao;
	private String nomeTabela;
	private String codigoDiagnostico;
	private String descricaoDiagnostico;
	private Integer motivoSaidaInternacao;
	private List<Despesa> despesas;
	private Double totalGeralDespesas;
	private String tipoFaturamento;
	private Double totalDiarias;
	private Double totalMateriais;
	private Double totalMedicamentos;
	private Double totalGeralGuia;

	public String getRegistroANSFontePagadora() {
		return registroANSFontePagadora;
	}

	public void setRegistroANSFontePagadora(String registroANSFontePagadora) {
		this.registroANSFontePagadora = registroANSFontePagadora;
	}

	public Date getDataEmissaoGuia() {
		return dataEmissaoGuia;
	}

	public void setDataEmissaoGuia(Date dataEmissaoGuia) {
		this.dataEmissaoGuia = dataEmissaoGuia;
	}

	public String getNumeroGuiaPrestador() {
		return numeroGuiaPrestador;
	}

	public void setNumeroGuiaPrestador(String numeroGuiaPrestador) {
		this.numeroGuiaPrestador = numeroGuiaPrestador;
	}

	public String getNumeroGuiaOperadora() {
		return numeroGuiaOperadora;
	}

	public void setNumeroGuiaOperadora(String numeroGuiaOperadora) {
		this.numeroGuiaOperadora = numeroGuiaOperadora;
	}

	public String getNumeroGuiaSolicitacao() {
		return numeroGuiaSolicitacao;
	}

	public void setNumeroGuiaSolicitacao(String numeroGuiaSolicitacao) {
		this.numeroGuiaSolicitacao = numeroGuiaSolicitacao;
	}

	public String getNumerocarteira() {
		return numerocarteira;
	}

	public void setNumerocarteira(String numerocarteira) {
		this.numerocarteira = numerocarteira;
	}

	public String getNomebeneficiario() {
		return nomebeneficiario;
	}

	public void setNomebeneficiario(String nomebeneficiario) {
		this.nomebeneficiario = nomebeneficiario;
	}

	public String getNomePlano() {
		return nomePlano;
	}

	public void setNomePlano(String nomePlano) {
		this.nomePlano = nomePlano;
	}

	public String getCodigoPrestadorOperadora() {
		return codigoPrestadorOperadora;
	}

	public void setCodigoPrestadorOperadora(String codigoPrestadorOperadora) {
		this.codigoPrestadorOperadora = codigoPrestadorOperadora;
	}

	public String getNomeContratado() {
		return nomeContratado;
	}

	public void setNomeContratado(String nomeContratado) {
		this.nomeContratado = nomeContratado;
	}

	public String getNumeroCNES() {
		return numeroCNES;
	}

	public void setNumeroCNES(String numeroCNES) {
		this.numeroCNES = numeroCNES;
	}

	public String getSenhaAutorizacao() {
		return senhaAutorizacao;
	}

	public void setSenhaAutorizacao(String senhaAutorizacao) {
		this.senhaAutorizacao = senhaAutorizacao;
	}

	public String getCaraterInternacao() {
		return caraterInternacao;
	}

	public void setCaraterInternacao(String caraterInternacao) {
		this.caraterInternacao = caraterInternacao;
	}

	public String getAcomodacao() {
		return acomodacao;
	}

	public void setAcomodacao(String acomodacao) {
		this.acomodacao = acomodacao;
	}

	public Date getDataHoraInternacao() {
		return dataHoraInternacao;
	}

	public void setDataHoraInternacao(Date dataHoraInternacao) {
		this.dataHoraInternacao = dataHoraInternacao;
	}

	public Date getDataHoraSaidaInterncao() {
		return dataHoraSaidaInterncao;
	}

	public void setDataHoraSaidaInterncao(Date dataHoraSaidaInterncao) {
		this.dataHoraSaidaInterncao = dataHoraSaidaInterncao;
	}

	public Integer getTipoInternacao() {
		return tipoInternacao;
	}

	public void setTipoInternacao(Integer tipoInternacao) {
		this.tipoInternacao = tipoInternacao;
	}

	public Integer getRegimeInternacao() {
		return regimeInternacao;
	}

	public void setRegimeInternacao(Integer regimeInternacao) {
		this.regimeInternacao = regimeInternacao;
	}

	public String getNomeTabela() {
		return nomeTabela;
	}

	public void setNomeTabela(String nomeTabela) {
		this.nomeTabela = nomeTabela;
	}

	public String getCodigoDiagnostico() {
		return codigoDiagnostico;
	}

	public void setCodigoDiagnostico(String codigoDiagnostico) {
		this.codigoDiagnostico = codigoDiagnostico;
	}

	public String getDescricaoDiagnostico() {
		return descricaoDiagnostico;
	}

	public void setDescricaoDiagnostico(String descricaoDiagnostico) {
		this.descricaoDiagnostico = descricaoDiagnostico;
	}

	public Integer getMotivoSaidaInternacao() {
		return motivoSaidaInternacao;
	}

	public void setMotivoSaidaInternacao(Integer motivoSaidaInternacao) {
		this.motivoSaidaInternacao = motivoSaidaInternacao;
	}

	public List<Despesa> getDespesas() {
		return despesas;
	}

	public void setDespesas(List<Despesa> despesas) {
		this.despesas = despesas;
	}

	public Double getTotalGeralDespesas() {
		return totalGeralDespesas;
	}

	public void setTotalGeralDespesas(Double totalGeralDespesas) {
		this.totalGeralDespesas = totalGeralDespesas;
	}

	public String getTipoFaturamento() {
		return tipoFaturamento;
	}

	public void setTipoFaturamento(String tipoFaturamento) {
		this.tipoFaturamento = tipoFaturamento;
	}

	public Double getTotalDiarias() {
		return totalDiarias;
	}

	public void setTotalDiarias(Double totalDiarias) {
		this.totalDiarias = totalDiarias;
	}

	public Double getTotalMateriais() {
		return totalMateriais;
	}

	public void setTotalMateriais(Double totalMateriais) {
		this.totalMateriais = totalMateriais;
	}

	public Double getTotalMedicamentos() {
		return totalMedicamentos;
	}

	public void setTotalMedicamentos(Double totalMedicamentos) {
		this.totalMedicamentos = totalMedicamentos;
	}

	public Double getTotalGeralGuia() {
		return totalGeralGuia;
	}

	public void setTotalGeralGuia(Double totalGeralGuia) {
		this.totalGeralGuia = totalGeralGuia;
	}

}
