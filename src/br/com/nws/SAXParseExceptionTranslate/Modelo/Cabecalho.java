package br.com.nws.SAXParseExceptionTranslate.Modelo;

import java.sql.Date;
import java.sql.Time;

public class Cabecalho {

	private String tipoTransacao;
	private Integer sequencialTransacao;
	private Date dataRegistroTransacao;
	private Time horaRegistroTransacao;
	private String codigoPrestadorNaOperadora;
	private String registroANS;
	private String versaoPadrao;
	private String nomeAplicativo;
	private String versaoAplicativo;
	private String fabricandoAplicativo;

	public String getTipoTransacao() {
		return tipoTransacao;
	}

	public void setTipoTransacao(String tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}

	public Integer getSequencialTransacao() {
		return sequencialTransacao;
	}

	public void setSequencialTransacao(Integer sequencialTransacao) {
		this.sequencialTransacao = sequencialTransacao;
	}

	public Date getDataRegistroTransacao() {
		return dataRegistroTransacao;
	}

	public void setDataRegistroTransacao(Date dataRegistroTransacao) {
		this.dataRegistroTransacao = dataRegistroTransacao;
	}

	public Time getHoraRegistroTransacao() {
		return horaRegistroTransacao;
	}

	public void setHoraRegistroTransacao(Time horaRegistroTransacao) {
		this.horaRegistroTransacao = horaRegistroTransacao;
	}

	public String getCodigoPrestadorNaOperadora() {
		return codigoPrestadorNaOperadora;
	}

	public void setCodigoPrestadorNaOperadora(String codigoPrestadorNaOperadora) {
		this.codigoPrestadorNaOperadora = codigoPrestadorNaOperadora;
	}

	public String getRegistroANS() {
		return registroANS;
	}

	public void setRegistroANS(String registroANS) {
		this.registroANS = registroANS;
	}

	public String getVersaoPadrao() {
		return versaoPadrao;
	}

	public void setVersaoPadrao(String versaoPadrao) {
		this.versaoPadrao = versaoPadrao;
	}

	public String getNomeAplicativo() {
		return nomeAplicativo;
	}

	public void setNomeAplicativo(String nomeAplicativo) {
		this.nomeAplicativo = nomeAplicativo;
	}

	public String getVersaoAplicativo() {
		return versaoAplicativo;
	}

	public void setVersaoAplicativo(String versaoAplicativo) {
		this.versaoAplicativo = versaoAplicativo;
	}

	public String getFabricandoAplicativo() {
		return fabricandoAplicativo;
	}

	public void setFabricandoAplicativo(String fabricandoAplicativo) {
		this.fabricandoAplicativo = fabricandoAplicativo;
	}

}
