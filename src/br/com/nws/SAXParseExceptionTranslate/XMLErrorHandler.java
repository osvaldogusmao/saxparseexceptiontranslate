package br.com.nws.SAXParseExceptionTranslate;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * 
 * <strong>XMLErrorHandler <br>
 * Classe respons�vel em tratar as exception lan�adas ao fazer um parse do XML
 * </strong>
 * 
 * @author Osvaldo Gusm�o
 * 
 * @category ErrorHandler
 * 
 * <br>
 *           <strong>How Use</strong><br>
 * 
 *           <code> XMLErrorHandler erroHandler = new XMLErrorHandler();
 * validator.setErrorHandler(erroHandler);
 * </code>
 * 
 * */
public class XMLErrorHandler implements ErrorHandler {

	private List<SAXParseException> errors = new ArrayList<SAXParseException>();

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		System.err.println(exception.getMessage());
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		errors.add(SAXParseExceptionTranslate.translate(exception));
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		throw exception;
	}

	public List<SAXParseException> getErrors() {
		return errors;
	}

}