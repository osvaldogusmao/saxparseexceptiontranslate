package br.com.nws.SAXParseExceptionTranslate;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.LocatorImpl;

public class SAXParseExceptionTranslate {

	/**
	 * 
	 * Attr
	 * 
	 * */
	private static LocatorImpl locator;

	/**
	 * <strong>Recebe a String de erro e traduz</strong>
	 * 
	 * @param exception
	 * 
	 * @return SAXParseException
	 * */
	public static SAXParseException translate(SAXParseException exception) {

		locator = new LocatorImpl();

		locator.setColumnNumber(exception.getColumnNumber());
		locator.setLineNumber(exception.getLineNumber());
		locator.setSystemId(exception.getSystemId());
		locator.setPublicId(exception.getPublicId());

		if (exception.getMessage().substring(0, 15).equals("cvc-enumeration")) {
			return translateErrorEnumeration(exception);
		}

		if (exception.getMessage().substring(0, 12).equals("cvc-datatype")) {
			return translateErrorDataType(exception);
		}

		if (exception.getMessage().substring(0, 12).equals("cvc-datatype")) {
			return translateErrorDataType(exception);
		}

		if (exception.getMessage().substring(0, 13).equals("cvc-minLength")) {
			return translateErrorMinLength(exception);
		}

		if (exception.getMessage().substring(0, 16).equals("cvc-complex-type")) {
			return translateErrorComplexType(exception);
		}

		return null; 
	}

	/**
	 * <strong>Traduz exception do tipo: cvc-enumeration-valid</strong>
	 * 
	 * @param exception
	 * */
	public static SAXParseException translateErrorEnumeration(
			SAXParseException exception) {

		String novaMensagem = "";

		novaMensagem = exception.getMessage().replace("cvc-enumeration-valid:",
				"Erro de conte�do: ");
		novaMensagem = novaMensagem.replace("Value", "O valor ");
		novaMensagem = novaMensagem
				.replace("is not facet-valid with respect to enumeration",
						" n�o � um valor v�lido em rela��o aos permitidos. S�o eles : ");
		novaMensagem = novaMensagem.replace(
				"It must be a value from the enumeration.", "");

		return new SAXParseException(novaMensagem, locator);
	}

	/**
	 * <strong>Traduz exception do tipo: cvc-datatype-valid</strong>
	 * 
	 * @param exception
	 * */
	public static SAXParseException translateErrorDataType(
			SAXParseException exception) {

		String novaMensagem = "";

		novaMensagem = exception.getMessage().replace(
				"cvc-datatype-valid.1.2.1:", "Erro de tipo de dados:");
		novaMensagem = novaMensagem.replace("is not a valid value for",
				"n�o � um valor correto para dados do tipo");

		return new SAXParseException(novaMensagem, locator);
	}

	/**
	 * <strong>Traduz exception do tipo: cvc-minLength-valid</strong>
	 * 
	 * @param exception
	 * */
	public static SAXParseException translateErrorMinLength(
			SAXParseException exception) {

		String novaMensagem = "";

		novaMensagem = exception.getMessage().replace("cvc-minLength-valid:",
				"Erro de tamanho de campo:");
		novaMensagem = novaMensagem.replace("Value", "O valor ");
		novaMensagem = novaMensagem.replace("with length", "com tamanho");
		novaMensagem = novaMensagem.replace(
				"is not facet-valid with respect to minLength",
				"n�o � um valor v�lido. Tamanho m�nimo:");
		novaMensagem = novaMensagem.replace("for type", "para o campo");

		return new SAXParseException(novaMensagem, locator);

	}

	/**
	 * <strong>Traduz exception do tipo: cvc-complex-type-valid</strong>
	 * 
	 * @param exception
	 * */
	public static SAXParseException translateErrorComplexType(
			SAXParseException exception) {

		String novaMensagem = "";

		novaMensagem = exception.getMessage().replace(
				"cvc-complex-type.2.4.a:", "Erro de estrutura:");
		novaMensagem = novaMensagem.replace(
				"Invalid content was found starting with element",
				"O campo obrigat�rio a seguir, n�o foi iniciado");
		novaMensagem = novaMensagem.replace("One of",
				"Insira um campo do tipo ");
		novaMensagem = novaMensagem.replace("is expected", "");

		return new SAXParseException(novaMensagem, locator);

	}

}